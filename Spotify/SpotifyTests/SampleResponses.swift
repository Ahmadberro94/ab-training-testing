//
//  SampleResponses.swift
//  SpotifyTests
//
//  Created by Ahmad Berro on 03/06/2021.
//

import Foundation

public enum SampleResponseType {
  case artist, album,findArtist
}

public var sampleResponses: [SampleResponseType: Data] = [
  .artist: """
{
"external_urls" : {
"spotify" : "https://open.spotify.com/artist/0OdUWJ0sBjDrqHygGUXeCF"
},
"followers" : {
"href" : null,
"total" : 306565
},
"genres" : [ "indie folk", "indie pop" ],
"href" : "https://api.spotify.com/v1/artists/0OdUWJ0sBjDrqHygGUXeCF",
"id" : "0OdUWJ0sBjDrqHygGUXeCF",
"images" : [ {
"height" : 816,
"url" : "https://i.scdn.co/image/eb266625dab075341e8c4378a177a27370f91903",
"width" : 1000
}, {
"height" : 522,
"url" : "https://i.scdn.co/image/2f91c3cace3c5a6a48f3d0e2fd21364d4911b332",
"width" : 640
}, {
"height" : 163,
"url" : "https://i.scdn.co/image/2efc93d7ee88435116093274980f04ebceb7b527",
"width" : 200
}, {
"height" : 52,
"url" : "https://i.scdn.co/image/4f25297750dfa4051195c36809a9049f6b841a23",
"width" : 64
} ],
"name" : "Band of Horses",
"popularity" : 59,
"type" : "artist",
"uri" : "spotify:artist:0OdUWJ0sBjDrqHygGUXeCF"
}
""".data(using: .utf8)!,
  .album: """
{
"album_type" : "album",
"album_group" : "album Group",
"artists" : [ {
"external_urls" : {
"spotify" : "https://open.spotify.com/artist/2BTZIqw0ntH9MvilQ3ewNY"
},
"href" : "https://api.spotify.com/v1/artists/2BTZIqw0ntH9MvilQ3ewNY",
"id" : "2BTZIqw0ntH9MvilQ3ewNY",
"name" : "Cyndi Lauper",
"type" : "artist",
"uri" : "spotify:artist:2BTZIqw0ntH9MvilQ3ewNY"
} ],
"external_urls" : {
"spotify" : "https://open.spotify.com/album/0sNOF9WDwhWunNAHPD3Baj"
},
"href" : "https://api.spotify.com/v1/albums/0sNOF9WDwhWunNAHPD3Baj",
"id" : "0sNOF9WDwhWunNAHPD3Baj",
"images" : [ {
"height" : 640,
"url" : "https://i.scdn.co/image/b6e762dcce1502ce63eb2c68798843eb2ed53c51",
"width" : 640
}, {
"height" : 300,
"url" : "https://i.scdn.co/image/66302ca80395b6be3600d5f0ef69db9e0c43f4f5",
"width" : 300
}, {
"height" : 64,
"url" : "https://i.scdn.co/image/e2e8cd6bf776613f9b84d1e4403a8abd51bb7234",
"width" : 64
} ],
"name" : "She's So Unusual",
"release_date" : "1983",
"release_date_precision" : "year",
"type" : "album",
"total_tracks": 1,
"uri" : "spotify:album:0sNOF9WDwhWunNAHPD3Baj"
}
""".data(using: .utf8)!
]

