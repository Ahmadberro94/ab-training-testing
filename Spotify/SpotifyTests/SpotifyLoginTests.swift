//
//  SpotifyTests.swift
//  SpotifyTests
//
//  Created by Ahmad Berro on 01/06/2021.
//

import XCTest
import Spotify

@testable import SpotifyLogin

class SpotifyLoginTests: XCTestCase {
  let spotifyRedirectUrl = "spotify://"
  func testURLParsing() {
    let urlBuilder = URLBuilder(clientID: "id", clientSecret: "secret", redirectURL: URL(string: "spotify.com")!)
    // Parse valid url
    let validURL = URL(string: "scheme://?code=spotify")!
    let parsedValidURL = urlBuilder.parse(url: validURL)
    XCTAssertFalse(parsedValidURL.error)
    XCTAssertEqual(parsedValidURL.code, "spotify")
    // Parse invalid url
    let invalidURL = URL(string: "http://scheme")!
    let parsedInvalidURL = urlBuilder.parse(url: invalidURL)
    XCTAssertTrue(parsedInvalidURL.error)
    XCTAssertNil(parsedInvalidURL.code)
  }
  
  func testCanHandleURL() {
    let urlBuilder = URLBuilder(clientID: "id", clientSecret: "secret", redirectURL: URL(string: spotifyRedirectUrl)!)
    // Handle valid URL
    let validURL = URL(string: spotifyRedirectUrl)!
    XCTAssertTrue(urlBuilder.canHandleURL(validURL))
    // Handle invalid URL
    let invalidURL = URL(string: "http://spotify.com")!
    XCTAssertFalse(urlBuilder.canHandleURL(invalidURL))
  }
  
  func testAuthenticationURL() {
    let urlBuilder = URLBuilder(clientID: "id", clientSecret: "secret", redirectURL: URL(string: spotifyRedirectUrl)!)
    let webAuthenticationURL = urlBuilder.authenticationURL(type: .web, scopes: [])
    XCTAssertNotNil(webAuthenticationURL)
    let appAuthenticationURL = urlBuilder.authenticationURL(type: .app, scopes: [.streaming])
    XCTAssertNotNil(appAuthenticationURL)
  }
  
  func testSessionValid() {
    let validSession = Session(username: "userName",
                               accessToken: "accessToken",
                               refreshToken: "refreshToken",
                               expirationDate: Date(timeIntervalSinceNow: 100))
    XCTAssertTrue(validSession.isValid())
    let inalidSession = Session(username: "userName",
                                accessToken: "accessToken",
                                refreshToken: "refreshToken",
                                expirationDate: Date(timeIntervalSinceNow: -100))
    XCTAssertFalse(inalidSession.isValid())
  }
  
  func testUsername() {
    let testUsername = "fakeUser"
    let session = Session(username: testUsername,
                          accessToken: "accessToken",
                          refreshToken: "refreshToken",
                          expirationDate: Date())
    SpotifyLogin.shared.session = session
    XCTAssertEqual(SpotifyLogin.shared.username, testUsername)
  }
  
  func testLogout() {
    let testSession = Session(username: "testUsername",
                              accessToken: "testToken",
                              refreshToken: "refreshToken",
                              expirationDate: Date.distantFuture)
    SpotifyLogin.shared.session = testSession
    SpotifyLogin.shared.logout()
    XCTAssertNil(SpotifyLogin.shared.session)
  }}
