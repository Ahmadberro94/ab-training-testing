//
//  SpotifyTests.swift
//  SpotifyTests
//
//  Created by Ahmad Berro on 01/06/2021.
//

import XCTest

@testable import Spotify

class SpotifyTests: XCTestCase {
  let resource = APIManager()
  // MARK: Spotify responses - decoding tests
  func testAlbum() {
    let album = try? JSONDecoder().decode(Item.self,
                                          from: sampleResponses[.album]!)
    XCTAssertNotNil(album)
  }
  
  
  func testArtist() {
    let artist = try? JSONDecoder().decode(Artists.self,
                                           from: sampleResponses[.artist]!)
    XCTAssertNotNil(artist)
  }
  

  
  
  
  
}
