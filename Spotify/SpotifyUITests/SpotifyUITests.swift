//
//  SpotifyUITests.swift
//  SpotifyUITests
//
//  Created by Ahmad Berro on 01/06/2021.
//

import XCTest

class SpotifyUITests: XCTestCase {
  override func setUp() {
    XCUIApplication().launch()
  }
  func testApp() {
    let app = XCUIApplication()
    if app.buttons["LoginButton"].exists {
      tapLoginButton()
      sleep(3)
    }
    tapAccountButton()
    sleep(3)
    tapLoginButton()
    sleep(3)
    tapSearchButton()
    tapSearchBarfunction()
    sleep(5)
    tapCollectionCell()
    sleep(5)
    tapBackButton()
    sleep(3)
    tapSearchButton()
    secondTestSearchBarfunction()
    sleep(5)
    tapCollectionCell()
    sleep(5)
    tapCollectionCell()
    sleep(5)
  }
  func tapSearchButton(){
    let rightNavBarButton = XCUIApplication().navigationBars.buttons["navbarRightItem"]
    XCTAssert(rightNavBarButton.exists)
    rightNavBarButton.tap()
  }
  func tapLoginButton() {
    let app = XCUIApplication()
    let loginButton = app.buttons["LoginButton"]
    loginButton.tap()
  }
  
  func secondTestSearchBarfunction(){
    let app = XCUIApplication()
    UIPasteboard.general.string = "Sia"
    let passwordSecureTextField = app.searchFields["SearchBar"]
    passwordSecureTextField.press(forDuration: 1.1)
    app.menuItems["Select All"].tap()
    passwordSecureTextField.press(forDuration: 1.1)
    app.menuItems["Paste"].tap()
    
  }
  func tapSearchBarfunction(){
    let app = XCUIApplication()
    UIPasteboard.general.string = "Siames"
    let passwordSecureTextField = app.searchFields["SearchBar"]
    passwordSecureTextField.press(forDuration: 1.1)
    app.menuItems["Paste"].tap()
  }
  func tapCollectionCell(){
    let app = XCUIApplication()
    let cell = app.collectionViews.cells.firstMatch
    cell.tap()
  }
  func tapBackButton(){
    let app = XCUIApplication()
    app.buttons["back"].tap()
  }
  func tapAccountButton(){
    let app = XCUIApplication()
    app.buttons["accountButton"].tap()
    sleep(2)
    app.buttons["logout"].tap()
    sleep(2)
    app.buttons["okAction"].tap()
  }
}
