//
//  SearchResultsResponse.swift
//  Spotify
//
//  Created by Ahmad Berro on 22/04/2021.
//
import Foundation

struct SearchResultsResponse: Codable {
    let artists: SearchArtistResponse
}


struct SearchArtistResponse: Codable {
    let items: [Artist]
}


