//
//  APIImage.swift
//  Spotify
//
//  Created by Ahmad Berro on 22/04/2021.
//
import Foundation

struct APIImage: Codable {
    let url: String
}

