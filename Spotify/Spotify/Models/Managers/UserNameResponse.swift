//
//  UserNameResponse.swift
//  Spotify
//
//  Created by Ahmad Berro on 06/07/2021.
//

import Foundation
struct UserNameResponse: Codable {
  let display_name: String
}


//struct UserNameResultsResponse: Codable {
//  let userName: SearchArtistResponse
//}
//
//
//struct UserNameResponseResponse: Codable {
//  let items: [Artist]
//}
