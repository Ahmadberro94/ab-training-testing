//
//  SearchResults.swift
//  Spotify
//
//  Created by Ahmad Berro on 22/04/2021.
//

import Foundation

enum SearchResult {
    case artist(model: Artist)
}

struct SearchSection {
    let title: String
    let results: [SearchResult]
}
