//
//  Artist.swift
//  Spotify
//
//  Created by Ahmad Berro on 26/04/2021.
//

import Foundation

struct Artist: Codable {
    let id: String
    let name: String
    let type: String
    let followers: APIFollowers
    let popularity: Int
    let images: [APIImage]?
    let external_urls: [String: String]
}
