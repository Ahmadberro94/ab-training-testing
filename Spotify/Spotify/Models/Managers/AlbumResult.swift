//
//  AlbumResult.swift
//  Spotify
//
//  Created by Ahmad Berro on 24/04/2021.
//

import Foundation

enum AlbumResult {
    case Album(model: Item)
}

struct AlbumSection {
    let title: String
    let results: [AlbumResult]
}
