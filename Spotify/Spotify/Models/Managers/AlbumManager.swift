//
//  AlbumOuter.swift
//  Spotify
//
//  Created by Ahmad Berro on 24/04/2021.
//

import Foundation

// MARK: - ArtistAlbum
struct ArtistAlbum: Codable {
    let href: String
    let items: [Item]
    let limit: Int
    let next: String
    let offset: Int
    let previous: String
    let total: Int
}

// MARK: - Item
struct Item: Codable {
    let albumGroup:  String
    let albumType: String
    let artists: [Artists]
    let external_urls: [String : String]
    let href: String
    let id: String
    let images: [Image]
    let name: String
    let releaseDate: String
    let releaseDatePrecision: String
    let totalTracks: Int
    let type: String
    let uri: String
    enum CodingKeys: String, CodingKey {
        case albumGroup = "album_group"
        case albumType = "album_type"
        case artists
        case external_urls = "external_urls"
        case href, id, images, name
        case releaseDate = "release_date"
        case releaseDatePrecision = "release_date_precision"
        case totalTracks = "total_tracks"
        case type, uri
    }
}

// MARK: - Artist
struct Artists: Codable {
    let external_urls: external_urls
    let href: String
    let id: String
    let name: String
    let type: String
    let uri: String
    enum CodingKeys: String, CodingKey {
        case external_urls = "external_urls"
        case href, id, name, type, uri
    }
}

// MARK: - external_urls
struct external_urls: Codable {
    let spotify: String
}

// MARK: - Image
struct Image: Codable {
    let height: Int
    let url: String
    let width: Int
}
