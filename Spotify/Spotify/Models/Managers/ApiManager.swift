////
////  ArtsitSearch.swift
////  Spotify
////
////  Created by Ahmad Berro on 22/04/2021.
////


import UIKit


final class APIManager {
  static let shared = APIManager()
  var spotifyName = ""
  enum HTTPMethod: String {
    case GET
    case POST
  }
  enum APIError: Error {
    case failedToGetData
  }
  // MARK: - Create Generic Request
  func createGenericAPIRequest(with url: URL?, type: HTTPMethod, completion: @escaping (URLRequest) -> Void) {
    AuthManager.shared.withValidToken { token in
      guard let apiURL = url else { return }
      var request = URLRequest(url: apiURL)
      request.setValue("Bearer \(token)",
                       forHTTPHeaderField: "Authorization")
      request.httpMethod = type.rawValue
      request.timeoutInterval = 30
      completion(request)
    }
  }
  
  //    MARK: - Get Album Api call
  public func getAlbumDetails(with artistsID: String, completion: @escaping (Result<[AlbumResult], Error>) -> Void) {
    let urlString = "/albums?offset=5&limit=50&include_groups=single,appears_on&market=ES"
    createGenericAPIRequest(with: URL(string: SpotifyConstants.baseAPIURL + "artists/" + (artistsID.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "") + urlString), type: .GET) { request in
      let task = URLSession.shared.dataTask(with: request) { data, _, error in
        guard let data = data, error == nil else {
          completion(.failure(APIError.failedToGetData))
          return
        }
        
        do {
          let result = try? JSONDecoder().decode(ArtistAlbum.self, from: data)
          // Convert to a string and print
          if let jsonString = String(data: data, encoding: String.Encoding.utf8) {
            print(jsonString)
          }
          if result != nil {
            var searchResults: [AlbumResult] = []
            searchResults.append(contentsOf: (result?.items.compactMap({  .Album(model: $0) }))!)
            completion(.success(searchResults))
            
          }else{
            let searchResults: [AlbumResult] = []
            completion(.success(searchResults))
          }
        }
      }
      task.resume()
    }
  }
  
  //    MARK: - Get Artist Api call
  public func search(with query: String, completion: @escaping (Result<[SearchResult], Error>) -> Void) {
    createGenericAPIRequest(with: URL(string: SpotifyConstants.baseAPIURL+"search?limit=10&type=artist,track&q=\(query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")"), type: .GET) { request in
      let task = URLSession.shared.dataTask(with: request) { data, _, error in
        guard let data = data, error == nil else {
          completion(.failure(APIError.failedToGetData))
          return
        }
        do {
          let result = try JSONDecoder().decode(SearchResultsResponse.self, from: data)
          
          // Convert to a string and print
          if let jsonString = String(data: data, encoding: String.Encoding.utf8) {
            print(jsonString)
          }
          var searchResults: [SearchResult] = []
          searchResults.append(contentsOf: result.artists.items.compactMap({ .artist(model: $0) }))
          
          completion(.success(searchResults))
          
        } catch {
          completion(.failure(error))
        }
      }
      task.resume()
    }
  }
  
  func fetchSpotifyProfile(accessToken: String,completion: @escaping (Result<String, Error>) -> Void) {
    let urlAccount = "https://api.spotify.com/v1/me"
    createGenericAPIRequest(with: URL(string: urlAccount ), type: .GET ) { request in
      let task = URLSession.shared.dataTask(with: request) { data, _, error in
        guard let data = data, error == nil else {
          completion(.failure(APIError.failedToGetData))
          return
        }
        do {
          let result = try JSONDecoder().decode(UserNameResponse.self, from: data)
          
          // Convert to a string and print
          if let jsonString = String(data: data, encoding: String.Encoding.utf8) {
            print(jsonString)
          }
          let spotifyName: String! = (result.display_name)
          completion(.success(spotifyName))
          
        } catch {
          completion(.failure(error))
        }
      }
      task.resume()
    }
    
  }
}
