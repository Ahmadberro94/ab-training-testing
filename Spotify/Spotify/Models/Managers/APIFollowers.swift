//
//  ApiFollowers.swift
//  Spotify
//
//  Created by Ahmad Berro on 23/04/2021.
//

import Foundation

struct APIFollowers: Codable {
    let total: Int
}
