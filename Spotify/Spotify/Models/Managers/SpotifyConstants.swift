
//
//  SpotifyConstants.swift
//  Spotify
//
//  Created by Ahmad Berro on 22/04/2021.
//

import Foundation


struct SpotifyConstants {
    static let baseAPIURL = "https://api.spotify.com/v1/"
}
