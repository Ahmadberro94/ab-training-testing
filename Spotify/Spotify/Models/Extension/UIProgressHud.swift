import UIKit

class ProgressHUD: UIVisualEffectView {
  
  var text: String? {
    didSet {
      label.text = text
    }
  }
  
  let activityIndictor: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
  let label: UILabel = UILabel()
  let blurEffect = UIBlurEffect(style: .light)
  let vibrancyView: UIVisualEffectView
  
  init(text: String) {
    self.text = text
    self.vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
    super.init(effect: blurEffect)
    self.setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    self.text = ""
    self.vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
    super.init(coder: aDecoder)
    self.setup()
  }
  
  func setup() {
    contentView.addSubview(vibrancyView)
    contentView.addSubview(activityIndictor)
    contentView.addSubview(label)
    activityIndictor.startAnimating()
  }
  
  override func didMoveToSuperview() {
    super.didMoveToSuperview()
    
    if let superview = self.superview {
      
      let width = 45
      let height = 45
      self.frame = CGRect(x: Int(superview.frame.size.width) / 2 - width / 2,
                          y: Int(superview.frame.height) / 2 - height / 2,
                          width: width,
                          height: height)
      vibrancyView.frame = self.bounds
      
      let activityIndicatorSize: CGFloat = 60
      activityIndictor.frame = CGRect(x: CGFloat(height / 2) - activityIndicatorSize / 2.1,
                                      y: CGFloat(width / 2) - activityIndicatorSize / 2.1,
                                      width: activityIndicatorSize,
                                      height: activityIndicatorSize)
      
      layer.cornerRadius = CGFloat(height / 2)
      layer.masksToBounds = true
    }
  }
  
  func show() {
    self.isHidden = false
  }
  
  func hide() {
    self.isHidden = true
  }
}
