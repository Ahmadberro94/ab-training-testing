import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

class ImageLoader: UIImageView {
  
  var imageURL: URL?
  
  let activityIndicator = UIActivityIndicatorView()
  
  func loadImageWithUrl(_ url: URL) {
    
    // setup activityIndicator...
    activityIndicator.color = .darkGray
    
    addSubview(activityIndicator)
    activityIndicator.translatesAutoresizingMaskIntoConstraints = false
    activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    
    imageURL = url
    
    image = nil
    activityIndicator.startAnimating()
    
    // retrieves image if already available in cache
    if let imageFromCache = imageCache.object(forKey: url as AnyObject) as? UIImage {
      
      self.image = imageFromCache
      activityIndicator.stopAnimating()
      return
    }
    
    // image does not available in cache.. so retrieving it from url...
    URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
      
      if error != nil {
        print(error as Any)
        DispatchQueue.main.async(execute: {
          self.activityIndicator.stopAnimating()
        })
        return
      }
      
      DispatchQueue.main.async(execute: {
        
        if let unwrappedData = data, let imageToCache = UIImage(data: unwrappedData) {
          
          if self.imageURL == url {
            self.image = imageToCache
          }
          
          imageCache.setObject(imageToCache, forKey: url as AnyObject)
        }
        self.activityIndicator.stopAnimating()
      })
    }).resume()
  }
}

extension UIImageView {
  func imageResize() {
    var contentClippingRect: CGRect {
      guard let image = image else { return bounds }
      guard contentMode == .scaleToFill else { return bounds }
      guard image.size.width > 0 && image.size.height > 0 else { return bounds }
      
      let scale: CGFloat
      if image.size.width > image.size.height {
        scale = bounds.width / image.size.width
      } else {
        scale = bounds.height / image.size.height
      }
      
      let size = CGSize(width: image.size.width * scale, height: image.size.height * scale)
      let x = (bounds.width - size.width) / 2.0
      let y = (bounds.height - size.height) / 2.0
      
      return CGRect(x: x, y: y, width: size.width, height: size.height)
    }
  }
  func setImageColor(color: UIColor) {
    let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
    self.image = templateImage
    self.tintColor = color
  }
  func makeRounded() {
    self.layer.masksToBounds = false
    self.layer.cornerRadius = (self.frame.height + self.frame.width) / 4
    self.clipsToBounds = true
  }
  func getImageSizeWithURL(url:String?) -> CGSize {
    var imageSize:CGSize = .zero
    guard let imageUrlStr = url else { return imageSize }
    guard imageUrlStr != "" else {return imageSize}
    guard let imageUrl = URL(string: imageUrlStr) else { return imageSize }
    
    guard let imageSourceRef = CGImageSourceCreateWithURL(imageUrl as CFURL, nil) else {return imageSize}
    guard let imagePropertie = CGImageSourceCopyPropertiesAtIndex(imageSourceRef, 0, nil)  as? Dictionary<String,Any> else {return imageSize }
    imageSize.width = CGFloat((imagePropertie[kCGImagePropertyPixelWidth as String] as! NSNumber).floatValue)
    imageSize.height = CGFloat((imagePropertie[kCGImagePropertyPixelHeight as String] as! NSNumber).floatValue)
    return imageSize
  }
}
extension UIImage
{
  /// Given a required height, returns a (rasterised) copy
  /// of the image, aspect-fitted to that height.
  
  func aspectFittedToHeight(_ newHeight: CGFloat) -> UIImage
  {
    let scale = newHeight / self.size.height
    let newWidth = self.size.width * scale
    let newSize = CGSize(width: newWidth, height: newHeight)
    let renderer = UIGraphicsImageRenderer(size: newSize)
    
    return renderer.image { _ in
      self.draw(in: CGRect(origin: .zero, size: newSize))
    }
  }
  var averageColor: UIColor? {
    guard let inputImage = CIImage(image: self) else { return nil }
    let extentVector = CIVector(x: inputImage.extent.origin.x, y: inputImage.extent.origin.y, z: inputImage.extent.size.width, w: inputImage.extent.size.height)
    
    guard let filter = CIFilter(name: "CIAreaAverage", parameters: [kCIInputImageKey: inputImage, kCIInputExtentKey: extentVector]) else { return nil }
    guard let outputImage = filter.outputImage else { return nil }
    
    var bitmap = [UInt8](repeating: 0, count: 4)
    let context = CIContext(options: [.workingColorSpace: kCFNull!])
    context.render(outputImage, toBitmap: &bitmap, rowBytes: 4, bounds: CGRect(x: 0, y: 0, width: 1, height: 1), format: .RGBA8, colorSpace: nil)
    
    return UIColor(red: CGFloat(bitmap[0]) / 255, green: CGFloat(bitmap[1]) / 255, blue: CGFloat(bitmap[2]) / 255, alpha: CGFloat(bitmap[3]) / 255)
  }
}

extension URL {
  func asyncDownload(completion: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> ()) {
    URLSession.shared
      .dataTask(with: self, completionHandler: completion)
      .resume()
  }
}
