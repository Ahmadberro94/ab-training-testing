//
//  ArtsitSearchModel.swift
//  Spotify
//
//  Created by Ahmad Berro on 22/04/2021.
//
import Foundation

struct ArtsitSearchModel {
    let id: String
    let title: String
    let imageURL: URL?
    let followers: Int
    let popularity: Int
}
