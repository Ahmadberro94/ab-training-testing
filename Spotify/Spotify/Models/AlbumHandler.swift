//
//  AlbumHandler.swift
//  Spotify
//
//  Created by Ahmad Berro on 03/06/2021.
//
import Foundation

struct AlbumHandler {
  
//  private let validation = LoginValidation()
//  private let loginApiResource = LoginApiResource()
  
  func authenticateUser(request: AlbumViewModel, completionHandler: @escaping(_ AlbumData: AlbumViewModel?)->()) {
    
    let validationResult = validation.validate(request: request)
    if(validationResult.isValid) {
      loginApiResource.authenticateUser(request: request) { (response) in
        // return it back to the caller
        completionHandler(LoginData(errorMessage: nil, response: response))
      }
    }else{
      completionHandler(LoginData(errorMessage: validationResult.message, response: nil))
    }
    
  }
}
