//
//  AlbumModel.swift
//  Spotify
//
//  Created by Ahmad Berro on 24/04/2021.
//

import Foundation

struct AlbumModel {
    let title: String
    let imageURL: URL?
    let name: String
    let releasedate: String
    let total: Int
    let type: String
}
