//
//  ViewController.swift
//  Spotify
//
//  Created by Ahmad Berro on 01/06/2021.
//

import UIKit

class NoResultController  {
  let imageViewObject = UIImageView()
  let spotifylabel = UILabel()
  func showimage(view: UIView,spotifyText: String,imageName: String,constraint: Int) {
    imageViewObject.image = UIImage(named: imageName)
    imageViewObject.setImageColor(color: .gray)
    spotifylabel.text = spotifyText
    spotifylabel.textColor = .lightGray
    imageViewObject.translatesAutoresizingMaskIntoConstraints = false
    spotifylabel.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(imageViewObject)
    view.addSubview(spotifylabel)
    NSLayoutConstraint.activate([
      imageViewObject.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      imageViewObject.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: CGFloat(constraint)),
      imageViewObject.heightAnchor.constraint(equalToConstant: 75),
      imageViewObject.widthAnchor.constraint(equalToConstant: 75)
    ])
    NSLayoutConstraint.activate([
      spotifylabel.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 10),
      spotifylabel.centerYAnchor.constraint(equalTo: imageViewObject.bottomAnchor, constant: 25),
      spotifylabel.heightAnchor.constraint(equalToConstant: 200),
      spotifylabel.widthAnchor.constraint(equalToConstant: 200)
    ])
    spotifylabel.isHidden = false
    imageViewObject.isHidden = false
  }
  func hideImage() {
    spotifylabel.isHidden = true
    imageViewObject.isHidden = true
  }
  
  func cleanInteger(followers: Int) -> String?{
    let largeNumber = followers
    let numberFormatter = NumberFormatter()
    numberFormatter.numberStyle = .decimal
    let formattedNumber = numberFormatter.string(from: NSNumber(value:largeNumber))
    let finalString = "\(formattedNumber ?? "0") followers"
    return finalString
  }
  
}
