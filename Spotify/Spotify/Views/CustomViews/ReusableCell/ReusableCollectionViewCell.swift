//
//  ReusableCollectionViewCell.swift
//  Spotify
//
//  Created by Ahmad Berro on 23/04/2021.
//


import UIKit
import Cosmos
class ReusableCollectionViewCell: UICollectionViewCell {
  
  
  @IBOutlet weak var rating: CosmosView!
  @IBOutlet weak var searchIconImageView: ImageLoader!
  @IBOutlet weak var constraint: UIView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var followersAndArtists: UILabel!
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var tracksLabel: UILabel!
  
  
  func showArtistData(){
    dateLabel.isHidden = true
    tracksLabel.isHidden = true
    rating.alpha = 1
  }
  func showAlbumsData(){
    dateLabel.isHidden = false
    tracksLabel.isHidden = false
    rating.alpha = 0
    
  }
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
      let targetSize = CGSize(width: layoutAttributes.frame.width, height: layoutAttributes.frame.height)
  layoutAttributes.frame.size = contentView.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel)
  return layoutAttributes
}
  override func prepareForReuse() {
    super.prepareForReuse()
    nameLabel.adjustsFontSizeToFitWidth = false
    searchIconImageView.imageResize()
    nameLabel?.text = nil
    searchIconImageView?.image = nil
    followersAndArtists?.text = nil
    dateLabel.text = nil
    tracksLabel.text = nil
    rating?.text = nil
    dateLabel.adjustsFontForContentSizeCategory = true
    tracksLabel.adjustsFontForContentSizeCategory = true
    nameLabel.adjustsFontForContentSizeCategory = true
  }
}
extension UICollectionView {
  var widestCellWidth: CGFloat {
    let insets = contentInset.top + contentInset.bottom
    return bounds.height - insets
  }
}
