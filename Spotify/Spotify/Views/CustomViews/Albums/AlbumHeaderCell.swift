//
//  AlbumHeaderCell.swift
//  Spotify
//
//  Created by Ahmad Berro on 22/06/2021.
//

import UIKit

class AlbumHeaderCell: UICollectionReusableView {
  @IBOutlet weak var imgView: ImageLoader!
  var artistNameLabel: UILabel = {
    let artistNameLabel: UILabel = UILabel()
    artistNameLabel.textColor = .white
    artistNameLabel.font = UIFont.systemFont(ofSize: 45, weight: .bold)
    artistNameLabel.numberOfLines = 2
    artistNameLabel.adjustsFontSizeToFitWidth = true
    artistNameLabel.minimumScaleFactor = 0.5
    artistNameLabel.layer.shadowRadius = 3.0
    artistNameLabel.layer.shadowOpacity = 1.0
    artistNameLabel.layer.shadowOffset = CGSize(width: 4, height: 4)
    artistNameLabel.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 0.01)
    return artistNameLabel
  }()
  
  var followersLabel: UILabel = {
    let followersLabel: UILabel = UILabel()
    followersLabel.textColor = .white
    followersLabel.font = UIFont.systemFont(ofSize: 17, weight: .regular)
    followersLabel.adjustsFontSizeToFitWidth = true
    followersLabel.minimumScaleFactor = 0.2
    followersLabel.numberOfLines = 0 // or 1
    return followersLabel
  }()
  var albumHeaderLabel: UILabel = {
    let albumHeaderLabel: UILabel = UILabel()
    albumHeaderLabel.textColor = .white
    albumHeaderLabel.font = UIFont.systemFont(ofSize: 27, weight: .regular)
    albumHeaderLabel.adjustsFontSizeToFitWidth = true
    albumHeaderLabel.minimumScaleFactor = 0.2
    albumHeaderLabel.numberOfLines = 0 // or 1
    return albumHeaderLabel
  }()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    artistNameLabel.text = "Albums"
    artistNameLabel.textColor = .white
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    
    addSubview(artistNameLabel)
    
    artistNameLabel.translatesAutoresizingMaskIntoConstraints = false
    artistNameLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor,  constant: -60).isActive = true
    artistNameLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
    artistNameLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
    
    addSubview(followersLabel)
    
    followersLabel.translatesAutoresizingMaskIntoConstraints = false
    followersLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -37).isActive = true
    followersLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
    followersLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    
    addSubview(albumHeaderLabel)
    
    albumHeaderLabel.translatesAutoresizingMaskIntoConstraints = false
    albumHeaderLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -2).isActive = true
    albumHeaderLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
    albumHeaderLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    
  }
}
