//
//  ArtistHeaderCell.swift
//  Spotify
//
//  Created by Ahmad Berro on 21/06/2021.
//
import UIKit

class ArtistHeaderCell: UICollectionReusableView {
  
  var artistHeaderLabel: UILabel = {
    let artistHeaderLabel: UILabel = UILabel()
    artistHeaderLabel.textColor = .white
    artistHeaderLabel.font = UIFont.systemFont(ofSize: 24, weight: .semibold)
    artistHeaderLabel.adjustsFontSizeToFitWidth = true
    artistHeaderLabel.minimumScaleFactor = 0.2
    artistHeaderLabel.numberOfLines = 0 // or 1
    return artistHeaderLabel
  }()
  override func awakeFromNib() {
    super.awakeFromNib()
    artistHeaderLabel.text = "Artists"
    artistHeaderLabel.textColor = .white
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    addSubview(artistHeaderLabel)
    
    artistHeaderLabel.translatesAutoresizingMaskIntoConstraints = false
    artistHeaderLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
    artistHeaderLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
    artistHeaderLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    artistHeaderLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: 0).isActive = true
  }
}
