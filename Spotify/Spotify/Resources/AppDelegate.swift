//
//  AppDelegate.swift
//  Spotify
//
//  Created by Ahmad Berro on 22/04/2021.
//

import UIKit
import SpotifyLogin
//import Firebase
@main
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    let redirectURL: URL = URL(string: "spotifycodetest://returnafterlogin")!
    SpotifyLogin.shared.configure(clientID: "7448f03e2a33482ca98c62cc18e7c0df",
                                  clientSecret: "12428b94d1184a0ea91a219b7730696a",
                                  redirectURL: redirectURL)
    window?.makeKeyAndVisible()
    return true
  }
  
  func application(_ app: UIApplication,
                   open url: URL,
                   options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
    let handled = SpotifyLogin.shared.applicationOpenURL(url) { _ in
      sleep(0)
    }
    return handled
  }
  
  
}
extension UIApplication {
  var statusBarUIView: UIView? {
    
    if #available(iOS 13.0, *) {
      let tag = 3848245
      
      let keyWindow = UIApplication.shared.connectedScenes
        .map({$0 as? UIWindowScene})
        .compactMap({$0})
        .first?.windows.first
      
      if let statusBar = keyWindow?.viewWithTag(tag) {
        return statusBar
      } else {
        let height = keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? .zero
        
        
        let statusBarView = UIView(frame: height)
        statusBarView.tag = tag
        statusBarView.layer.zPosition = 999999
        
        keyWindow?.addSubview(statusBarView)
        return statusBarView
      }
      
    } else {
      
      if responds(to: Selector(("statusBar"))) {
        return value(forKey: "statusBar") as? UIView
      }
    }
    return nil
  }
}

