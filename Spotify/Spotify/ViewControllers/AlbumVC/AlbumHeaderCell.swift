//
//  ArtistHeaderCell.swift
//  Spotify
//
//  Created by Ahmad Berro on 21/06/2021.
//
import UIKit

class AlbumHeaderCell: UICollectionReusableView {
  
  var label: UILabel = {
    let label: UILabel = UILabel()
    label.textColor = .white
    label.font = UIFont.systemFont(ofSize: 30, weight: .semibold)
    label.sizeToFit()
    return label
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    addSubview(label)
    
    label.translatesAutoresizingMaskIntoConstraints = false
    label.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
    label.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 5).isActive = true
    label.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
