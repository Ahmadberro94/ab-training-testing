//
//  SearchViewController.swift
//  Spotify
//
//  Created by Ahmad Berro on 23/04/2021.
//

import UIKit
import SafariServices
import SpotifyLogin
//import Firebase


class SearchViewController : UIViewController {
  private var sectionResults: [SearchSection] = []
  var viewcontroller = NoResultController()
  @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
  @IBOutlet var profileView: UIBarButtonItem!
  @IBOutlet weak var collectionView: UICollectionView!
  let width = UIScreen.main.bounds.width
  let height = UIScreen.main.bounds.height
  var searchBar = UISearchBar()
  var searchBarButtonItem: UIBarButtonItem?
  var logoImageView   : UILabel!
  //MARK: - loading Views
  override func viewDidLoad() {
    super.viewDidLoad()
    let flowLayout = collectionView?.collectionViewLayout as? AlignedCollectionViewFlowLayout
    flowLayout?.verticalAlignment = .center
    NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(showLoginFlow), name: NSNotification.Name(rawValue:  "PeformAfterPresenting"), object: nil)
    
    self.setupUI()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    //if refresh token fails redirect the user to main page to login again and generate new token.
    SpotifyLogin.shared.getAccessToken { [weak self] (token, error) in
      UserDefaults.standard.setValue(token,forKey: "access_token")
      if error != nil || token == nil {
        self?.showLoginFlow()
      }else{
        APIManager.shared.fetchSpotifyProfile(accessToken: token!){ result in
          DispatchQueue.main.async { [self] in
            switch result {
            case .success(let results):
              
              UserDefaults.standard.set(results, forKey: "userName")
              
            case .failure(let error):
              print(error.localizedDescription)
            }
          }
        }
      }
    }
  }
  
  @objc func showLoginFlow() {
    self.performSegue(withIdentifier: "home_to_login", sender: self)
  }
  override func viewWillLayoutSubviews() {
    let layout = collectionView.collectionViewLayout
    if let flowLayout = layout as? AlignedCollectionViewFlowLayout {
      flowLayout.verticalAlignment = .top
      flowLayout.estimatedItemSize = CGSize(
        width: (collectionView.bounds.width / 2.2),
        // Make the height a reasonable estimate to
        // ensure the scroll bar remains smooth
        height: (collectionView.widestCellWidth / 2.5)
      )
      collectionView.layoutIfNeeded()
    }
    
  }
  override func viewWillAppear(_ animated: Bool) {
    self.navigationItem.setHidesBackButton(false, animated: true)
    let nib = UINib(nibName: "ReusableCollectionViewCell", bundle: nil)
    collectionView.register(nib, forCellWithReuseIdentifier: "ReusableCollectionViewCell")
    setupNavigationBar()
  }
  
  @IBAction func accountActionButton(_ sender: Any) {
    let dictionary = Bundle.main.infoDictionary!
    let version = dictionary["CFBundleShortVersionString"] as! String
    let build = dictionary["CFBundleVersion"] as! String
    let username =  UserDefaults.standard.string(forKey: "userName")
    let actionSheetTitle =  username!
    let actionSheetMessage = "Version " + version + "(\(build))"
    let optionMenu = UIAlertController(title: actionSheetTitle , message: actionSheetMessage , preferredStyle: .actionSheet)
    let logoutAction = UIAlertAction(title: "Logout", style: .destructive, handler:
                                      {
                                        (alert: UIAlertAction!) -> Void in
                                        let alertController = UIAlertController(title: "Spotify", message: "Are you sure you want to logout?", preferredStyle: .alert)
                                        
                                        // Create the actions
                                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                          UIAlertAction in
                                          SpotifyLogin.shared.logout()
                                          self.showLoginFlow()
                                        }
                                        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                                          UIAlertAction in
                                          NSLog("Cancel Pressed")
                                        }
                                        
                                        // Add the actions
                                        okAction.accessibilityIdentifier = "okAction"
                                        alertController.addAction(okAction)
                                        alertController.addAction(cancelAction)
                                        
                                        // Present the controller
                                        self.present(alertController, animated: true, completion: nil)
                                      })
    logoutAction.accessibilityIdentifier = "logout"
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
                                      {
                                        (alert: UIAlertAction!) -> Void in
                                        print("Cancelled")
                                      })
    optionMenu.addAction(logoutAction)
    optionMenu.addAction(cancelAction)
    self.present(optionMenu, animated: true, completion: nil)
  }
  
  //MARK: - GUI Setup
  
  func setupNavigationBar(){
    navigationItem.title = "Artists"
    searchBar.delegate = self
    searchBar.searchBarStyle = UISearchBar.Style.minimal
    searchBar.searchBarStyle = UISearchBar.Style.minimal
    searchBar.searchTextField.attributedPlaceholder = NSAttributedString(string: "Search Artist", attributes: [NSAttributedString.Key.foregroundColor : UIColor.init(white: 1, alpha: 0.7)])
    searchBar.restorationIdentifier = "SearchBar"
    searchBar.searchTextField.accessibilityIdentifier = "SearchBar"
    self.searchBar.accessibilityLabel = "SearchBar"
    self.searchBar.accessibilityIdentifier = "SearchBar"
    searchBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.3738358858)
    searchBarButtonItem = navigationItem.rightBarButtonItem
    self.navigationItem.setLeftBarButton(self.profileView, animated: true)
    self.profileView.accessibilityIdentifier = "accountButton"
    self.navigationItem.setRightBarButton(self.searchBarButtonItem, animated: true)
    if let navController = navigationController {
      navController.navigationBar.tintColor = .white
      navController.navigationBar.setBackgroundImage(UIImage(), for: .default)
      navController.navigationBar.shadowImage = UIImage()
      navController.isNavigationBarHidden = false
      navController.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5)
      navController.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5)
      let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white,NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 25)]
      navController.navigationBar.titleTextAttributes = textAttributes
    }
    
  }
  func showSearchBar() {
    searchBar.alpha = 0
    navigationItem.titleView = searchBar
    navigationItem.setLeftBarButton(nil, animated: true)
    navigationItem.setRightBarButton(nil, animated: true)
    searchBar.setShowsCancelButton(true, animated: true)
    UIView.animate(withDuration: 0.7, animations: {
      self.searchBar.alpha = 1
    }, completion: { finished in
      self.searchBar.becomeFirstResponder()
    })
  }
  func hideSearchBar() {
    self.searchBar.alpha = 1
    UIView.animate(withDuration: 0.4, animations: {
      self.searchBar.alpha = 0
    }, completion: { finished in
      
      let text = UILabel()
      text.attributedText = NSAttributedString(string: "Artists", attributes: [NSAttributedString.Key.foregroundColor : UIColor.init(white: 1, alpha: 1),NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 25)])
      self.navigationItem.titleView? = text
      self.navigationItem.setLeftBarButton(self.profileView, animated: true)
      self.navigationItem.setRightBarButton(self.searchBarButtonItem, animated: true)
      
    })
  }
  
  func setupUI(){
    viewcontroller.showimage(view: view, spotifyText: "Please enter Artist Name",imageName: "Search",constraint: 15)
    setGradientBackground()
  }
  
  func setGradientBackground() {
    let screenSize: CGRect = UIScreen.main.bounds
    let colorTop = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
    let colorBottom = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    let gradientLayer = CAGradientLayer()
    gradientLayer.colors = [colorTop.cgColor, colorBottom]
    gradientLayer.locations = [0.0, 0.9]
    let screenWidth = screenSize.width
    let screenHeight = screenSize.height
    gradientLayer.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight + 40)
    
    self.view.layer.insertSublayer(gradientLayer, at: 0)
  }
  
  @IBAction func searchButtonPressed(sender: AnyObject) {
    showSearchBar()
  }
  //MARK: - Keyboard Handler
  
  @objc func handleKeyboardNotification(notification: NSNotification) {
    
    if let userInfo = notification.userInfo {
      
      _ = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
      let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
      UIView.animate(withDuration: 0, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
        
        self.view.layoutIfNeeded()
        
      }, completion: { (completed) in
        
        if isKeyboardShowing {
          guard let userInfo = notification.userInfo else { return }
          var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
          keyboardFrame = self.view.convert(keyboardFrame, from: nil)
          
          var contentInset:UIEdgeInsets = self.collectionView.contentInset
          contentInset.bottom = keyboardFrame.size.height + 40
          self.collectionView.contentInset = contentInset
          let indexPath = NSIndexPath(item: 10, section: 0)
          self.collectionView?.scrollToItem(at: indexPath as IndexPath, at: .top, animated: true)
        }
        else{
          
          let contentInset:UIEdgeInsets = UIEdgeInsets.zero
          self.collectionView.contentInset = contentInset
        }
      })
    }
  }
}


//MARK: - SearchBar Delegate

extension SearchViewController: UISearchResultsUpdating, UISearchBarDelegate  {
  func updateSearchResults(for searchController: UISearchController) {
    //for updates when search bar content changes
  }
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    hideSearchBar()
    searchBar.setShowsCancelButton(true, animated: true)
    
  }
  func update(with results: [SearchResult]) {
    self.collectionView.delegate = self
    self.collectionView.dataSource = self
    let artists = results.filter({
      switch $0 {
      case .artist: return true
      }
    })
    
    self.sectionResults = [
      SearchSection(title: "Artists", results: artists),
    ]
    
    collectionView.layoutIfNeeded()
    collectionView.reloadData()
    collectionView.layoutIfNeeded()
    collectionView.isHidden = results.isEmpty // hide results tableview if results empty
  }
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
    
    collectionView.reloadData()
  }
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    
    let query = searchBar.text
    APIManager.shared.search(with: query!) { result in
      DispatchQueue.main.async { [self] in
        switch result {
        case .success(let results):
          update(with: results)
        case .failure(let error):
          print(error.localizedDescription)
        }
      }
    }
    searchBar.resignFirstResponder()
  }
  //    MARK: - Search For Artists funtion
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    let query = searchText
    if query != ""{
      APIManager.shared.search(with: query) { result in
        DispatchQueue.main.async { [self] in
          switch result {
          case .success(let results):
            viewcontroller.hideImage()
            update(with: results)
            collectionView.reloadData()
          case .failure(let error):
            SpotifyLogin.shared.getAccessToken { [weak self] (token, error) in
              UserDefaults.standard.setValue(token,forKey: "access_token")
              //if refresh token fails redirect the user to main page to login again and generate new token.
              if error != nil, token == nil {
                self?.showLoginFlow()
              }
            }
            print(error.localizedDescription)
          }
        }
      }
    }else{
      
      viewcontroller.showimage(view: view, spotifyText: "Please enter Artist Name",imageName: "Search",constraint: 15)
      self.sectionResults.removeAll()
      self.collectionView.reloadData()
      
    }
  }
}
//    MARK: - CollectionView Delegate
extension SearchViewController:UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if sectionResults.isEmpty == false {
      return sectionResults[section].results.count
    }
    return 0
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let result = sectionResults[indexPath.section].results[indexPath.row]
    switch result {
    case .artist(let artist):
      guard let artistCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReusableCollectionViewCell", for: indexPath) as? ReusableCollectionViewCell else { return  UICollectionViewCell() }
      let viewModel = ArtsitSearchModel(id: artist.id, title: artist.name, imageURL: URL(string: artist.images?.first?.url ?? ""), followers: artist.followers.total ,popularity: artist.popularity)
      artistCell.showArtistData()
      artistCell.nameLabel?.text = viewModel.title
      let followers = viewcontroller.cleanInteger(followers: viewModel.followers)
      artistCell.followersAndArtists?.text = followers
      artistCell.rating.settings.fillMode = .half
      artistCell.rating.settings.starSize = 16.5
      artistCell.rating.settings.totalStars = 5
      artistCell.rating.rating = 0.0
      let rating = Double(artist.popularity/20)
      artistCell.rating.rating = rating
      if viewModel.imageURL != nil {
        // unwrapped url safely...
        let url = viewModel.imageURL!.absoluteString
        
        if let strUrl = url.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
           let imgUrl = URL(string: strUrl) {
          
          artistCell.searchIconImageView.loadImageWithUrl(imgUrl) // call this line for getting image to yourImageView
        }
        
      }else{
        artistCell.searchIconImageView?.image = UIImage(named: "music")
      }
      artistCell.layoutIfNeeded()
      return artistCell
    }
  }
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
    let result = sectionResults[indexPath.section].results[indexPath.row]
    
    switch result {
    case .artist(let artist):
      let viewModel = ArtsitSearchModel(id: artist.id, title: artist.name, imageURL: URL(string: artist.images?.first?.url ?? ""), followers: artist.followers.total ,popularity: artist.popularity)
      let vc = self.storyboard?.instantiateViewController(withIdentifier: "AlbumViewController") as! AlbumViewController
      vc.artists = viewModel
      self.navigationController?.pushViewController(vc, animated:true)
      self.hideSearchBar()
      self.navigationController?.setNavigationBarHidden(false, animated: true)
      
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
  {
    let value = (width * 0.2) * 0.15
    return .init(top: 2, left: value, bottom: 0, right: value)
  }
  
  func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
    switch kind {
    case UICollectionView.elementKindSectionHeader:
      guard
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ArtistHeaderCell", for: indexPath) as? ArtistHeaderCell
      else {
        fatalError("Invalid view type")
      }
      headerView.backgroundColor = UIColor.clear
      if sectionResults.count == 0 || sectionResults.isEmpty {
        headerView.artistHeaderLabel.text = ""
        
      }else{
        headerView.artistHeaderLabel.text = "Artists"
      }
      return headerView
    default:
      assert(false, "Invalid element type")
    }
    fatalError("Unexpected element kind")
  }
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let pan = scrollView.panGestureRecognizer
    let velocity = pan.velocity(in: scrollView).y
    print(pan)
    print(velocity)
    if velocity < -5 {
      navigationController?.setNavigationBarHidden(true, animated: true)
    } else if velocity > 5 {
      self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    if velocity == 0 {
      searchBar.becomeFirstResponder()
    }
  }
}
