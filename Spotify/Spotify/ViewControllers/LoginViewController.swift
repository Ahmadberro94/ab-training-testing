//
//  SpotifyConstants.swift
//  Spotify
//
//  Created by Ahmad Berro on 22/04/2021.
//

import UIKit
import WebKit
import SpotifyLogin

class LoginViewController: UIViewController {
  var loginButton: UIButton?
  
  @IBOutlet weak var trailingMapConstraint: NSLayoutConstraint!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let button = SpotifyLoginButton(viewController: self,
                                    scopes: [.streaming,
                                             .userReadTop,
                                             .playlistReadPrivate,
                                             .userLibraryRead])
    
    self.view.addSubview(button)
    loginButton?.accessibilityIdentifier? = "LoginButton"
    loginButton?.restorationIdentifier = "LoginButton"
    loginButton?.accessibilityIdentifier = "LoginButton"
    self.loginButton = button
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(loginSuccessful),
                                           name: .SpotifyLoginSuccessful,
                                           object: nil)
    
    
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    navigationController?.setNavigationBarHidden(true, animated: animated)
    animateBackground()
  }
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    loginButton?.center = self.view.center
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  @objc func loginSuccessful() {
    self.navigationController?.popViewController(animated: true)
  }
  
  func animateBackground() {
    let viewThird = self.view.frame.width / 3
    self.trailingMapConstraint.constant = self.trailingMapConstraint.constant - viewThird
    UIView.animate(withDuration: 20, delay: 0, options: .curveEaseInOut, animations: {
      self.view.layoutIfNeeded()
    }) { _ in
      self.trailingMapConstraint.constant = self.trailingMapConstraint.constant + viewThird
      UIView.animate(withDuration: 20, delay: 0, options: .curveEaseInOut, animations: {
        self.view.layoutIfNeeded()
      })
    }
  }
  
}

