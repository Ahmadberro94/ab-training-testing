//
//  AlbumViewController.swift
//  Spotify
//
//  Created by Ahmad Berro on 22/06/2021.
//

import UIKit
import SafariServices
//import Firebase

class AlbumViewController: UIViewController, UIGestureRecognizerDelegate {
  var artists : ArtsitSearchModel?
  var sectionResults: ArtistAlbum?
  var viewController = NoResultController()
  var sectionAlbum: [AlbumSection] = []
  let progressHUD = ProgressHUD(text: "")
  @IBOutlet weak var backButton: UIButton!
  @IBOutlet weak var collectionView: UICollectionView!
  var navColor = UIColor()
  let width = UIScreen.main.bounds.width
  let height = UIScreen.main.bounds.height
  // MARK: view loading functions
  override func viewDidLoad() {
    super.viewDidLoad()
    self.backButton.layer.cornerRadius = (backButton.frame.height + backButton.frame.width) / 4
    self.backButton.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.2)
    self.backButton.tintColor = .black
    self.backButton.accessibilityIdentifier = "back"
    progressHUD.activityIndictor.color = .white
    progressHUD.activityIndictor.center = view.center
    progressHUD.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.0888865894)
    self.view.addSubview(self.progressHUD)
    if let navController = navigationController {
      navController.navigationBar.tintColor = .white
      navController.navigationBar.setBackgroundImage(UIImage(), for: .default)
      navController.navigationBar.shadowImage = UIImage()
      let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white,NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 18)]
      navController.navigationBar.titleTextAttributes = textAttributes
    }
    self.view.backgroundColor = UIColor.black
    self.navigationController?.interactivePopGestureRecognizer?.delegate = self
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(true)
    albumSearch()
    collectionView.delegate = self
    collectionView.dataSource = self
    let nib = UINib(nibName: "ReusableCollectionViewCell", bundle: nil)
    collectionView.register(nib, forCellWithReuseIdentifier: "ReusableCollectionViewCell")
    let layout = collectionView.collectionViewLayout
    if let flowLayout = layout as? AlignedCollectionViewFlowLayout {
      flowLayout.verticalAlignment = .top
      flowLayout.estimatedItemSize = CGSize(
        width: (collectionView.bounds.width / 2.2),
        // Make the height a reasonable estimate to
        // ensure the scroll bar remains smooth
        height: (collectionView.bounds.height / 3.5)
      )
    }
    collectionView.layoutIfNeeded()
  }
  
  func poptoView ()  {
    self.navigationController?.navigationBar.tintColor = UIColor(hue: 0, saturation: 0, brightness: 0, alpha: 0)
    self.navigationController?.navigationBar.backgroundColor = .clear
    UIApplication.shared.statusBarUIView?.backgroundColor =  .clear
    _ = navigationController?.popViewController(animated: true)
  }
  func setBackgroundGradiant(color : UIColor){
    let colorBottom = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0).cgColor
    print(color.cgColor)
    let gradientLayer = CAGradientLayer()
    gradientLayer.colors = [color.cgColor, colorBottom]
    gradientLayer.locations = [0.0, 0.6]
    gradientLayer.frame = self.view.bounds
    self.view.layer.insertSublayer(gradientLayer, at: 0)
  }
  
  @IBAction func backAction(_ sender: Any) {
    poptoView()
  }
  
  
  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    self.navigationController?.navigationBar.tintColor = UIColor(hue: 0, saturation: 0, brightness: 0, alpha: 0)
    self.navigationController?.navigationBar.backgroundColor = .clear
    UIApplication.shared.statusBarUIView?.backgroundColor =  .clear
    return true
  }
  
  // MARK: fetching data for albums
  func albumSearch() {
    DispatchQueue.main.async { [self] in
      APIManager.shared.getAlbumDetails(with: artists!.id) { result in
        switch result {
        case .success(let model):
          update(with: model)
        case .failure(let error):
          print(error.localizedDescription)
        }
      }
    }
  }
  func update(with results: [AlbumResult]) {
    DispatchQueue.main.async { [self] in
      
      let url = artists?.imageURL
      if url != nil {
        
        let _: ()? = url?.asyncDownload{ data, response , error in
          let image = UIImage(data: data!)
          let color = image?.averageColor
          self.navColor = color!
          self.setBackgroundGradiant(color: color!)
        }
        
      }else{
        _ = UIImage(named: "music")
        let color = UIColor.white
        setBackgroundGradiant(color: color)
      }
      
      self.collectionView.delegate = self
      self.collectionView.dataSource = self
      let album = results.filter({
        switch $0 {
        case .Album: return true
        }
      })
      
      self.sectionAlbum = [
        AlbumSection(title: "Artists", results: album),
      ]
      let layout = collectionView.collectionViewLayout
      if let flowLayout = layout as? AlignedCollectionViewFlowLayout {
        flowLayout.estimatedItemSize = CGSize(
          width: (collectionView.bounds.width / 2.2),
          // Make the height a reasonable estimate to
          // ensure the scroll bar remains smooth
          height: (collectionView.bounds.height / 3.5)
        )
      }
      
      collectionView.layoutIfNeeded()
      collectionView.reloadData()
      collectionView.layoutIfNeeded()
      progressHUD.removeFromSuperview()
      if results.isEmpty {
        viewController.spotifylabel.isHidden = true
        viewController.showimage(view: view, spotifyText: "This Artist has no albums",imageName: "Albums",constraint: 85)
      }else{
        collectionView.layoutIfNeeded()
        collectionView.reloadData()
        collectionView.layoutIfNeeded()
        viewController.hideImage()
      }
    }
  }
  func didTapResult(_ result: AlbumResult) {
    switch result {
    case .Album(let model):
      guard let url = URL(string: model.external_urls["spotify"] ?? "") else {
        return
      }
      let vc = SFSafariViewController(url: url)
      present(vc, animated: true)
      
    }
  }
  
}
// MARK: CollectionView Delegates
extension AlbumViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if sectionAlbum.isEmpty == false {
      return sectionAlbum[section].results.count
    }
    return 0
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let result = sectionAlbum[indexPath.section].results[indexPath.row]
    switch result {
    case .Album(let album):
      guard let albumCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReusableCollectionViewCell", for: indexPath) as? ReusableCollectionViewCell else { return  UICollectionViewCell() }
      let viewModel = AlbumModel(title: album.name, imageURL: URL(string: album.images.first?.url ?? "")!, name: album.name, releasedate: album.releaseDate, total: album.totalTracks,type: album.type)
      albumCell.showAlbumsData()
      let artistName = album.artists[indexPath.section].name
      albumCell.followersAndArtists.text =  artistName
      albumCell.nameLabel?.text = viewModel.name
      
      albumCell.dateLabel.text = viewModel.releasedate
      albumCell.tracksLabel.text = String(viewModel.total) + " Tracks"
      if viewModel.imageURL != nil {
        // unwrapped url safely...
        let url = viewModel.imageURL!.absoluteString
        
        if let strUrl = url.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
           let imgUrl = URL(string: strUrl) {
          
          albumCell.searchIconImageView.loadImageWithUrl(imgUrl) // call this line for getting image to yourImageView
        }
      }else{
        albumCell.searchIconImageView?.image = UIImage(named: "music")
      }
      albumCell.layoutIfNeeded()
      return albumCell
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let result = sectionAlbum[indexPath.section].results[indexPath.row]
    didTapResult(result)
    
  }
  func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
    switch kind {
    case UICollectionView.elementKindSectionHeader:
      guard
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AlbumHeadCell", for: indexPath) as? AlbumHeaderCell
      else {
        fatalError("Invalid view type")
      }
      headerView.artistNameLabel.text = artists!.title
      headerView.artistNameLabel.adjustsFontSizeToFitWidth = true
      let followers = viewController.cleanInteger(followers: artists!.followers)
      headerView.followersLabel.text = followers
      headerView.albumHeaderLabel.isHidden = false
      headerView.albumHeaderLabel.text = "Albums"
      if artists?.imageURL != nil {
        // unwrapped url safely...
        let url = artists?.imageURL?.absoluteString
        if let strUrl = url!.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
           let imgUrl = URL(string: strUrl) {
          headerView.imgView.loadImageWithUrl(imgUrl) // call this line for getting image to yourImageView
        }
      }else{
        headerView.imgView.image = UIImage(named: "music")
      }
      
      return headerView
    default:
      assert(false, "Invalid element type")
    }
    fatalError("Unexpected element kind")
  }
}
extension AlbumViewController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    
    let value = (width * 0.2) * 0.15
    return .init(top: 5, left: value, bottom: 5, right: value)
  }
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let offset = scrollView.contentOffset.y / 150
    if offset > 2 {
      title = artists?.title
      self.backButton.tintColor = .white
      self.backButton.layer.cornerRadius = (backButton.frame.height + backButton.frame.width) / 4
      self.backButton.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
      self.navigationController?.navigationBar.tintColor = UIColor(hue: 1, saturation: offset, brightness: 0.5, alpha: 0.5)
      
      self.navigationController?.navigationBar.backgroundColor =  navColor.withAlphaComponent(offset - 1.2)
      let heights = view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
      UIApplication.shared.statusBarUIView?.frame = CGRect(x: 0, y: 0, width: width, height: heights + 10)
      UIApplication.shared.statusBarUIView?.backgroundColor =  navColor.withAlphaComponent(offset - 1.2)
    }
    else{
      title = ""
      self.backButton.layer.cornerRadius = (backButton.frame.height + backButton.frame.width) / 4
      self.backButton.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: offset + 0.2)
      self.backButton.tintColor = .white
      self.navigationController?.navigationBar.tintColor = UIColor(hue: 1, saturation: offset, brightness: 0.5, alpha: 0.5)
      self.navigationController?.navigationBar.backgroundColor = navColor.withAlphaComponent(offset - 1.2)
      UIApplication.shared.statusBarUIView?.backgroundColor = navColor.withAlphaComponent(offset - 1.2)
    }
  }
}
